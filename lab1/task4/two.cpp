//
// Created by main on 27.04.2021.
//

//
// Created by main on 26.04.2021.
//

#include <omp.h>
#include<stdio.h>
#include<time.h>

#define M 1000

int main() {

  float A[M][M], b[M], c[M];
  int i, j, rank, size;

  for (i = 0; i < M; i++) {
    for (j = 0; j < M; j++)
      A[i][j] = (j + 1) * 1.0;
    b[i] = 1.0 * (i + 1);
    c[i] = 0.0;
  }

  /*printf("Вывод значений матрицы A и вектора b на экран:\n");

  for (i = 0; i < M; i++) {
    printf(" A[%d]= ", i);
    for (j = 0; j < M; j++)
      printf("%.1f ", A[i][j]);
    printf(" b[%d]= %.1f\n", i, b[i]);
  }*/
  clock_t start_time =  clock();
  omp_set_num_threads(2);
#pragma omp parallel shared(A, b, c) private(rank, i, j)
  {
    rank = omp_get_thread_num();
#pragma omp sections nowait
    {
#pragma omp section
      {
        for (i = 0; i < M / 2; i++) {
          for (j = 0; j < M; j++)
            c[i] += (A[i][j] * b[j]);
          //printf("rank= %d i= %d c[%i]=%.2f\n", rank, i, i, c[i]);
        }
      }
#pragma omp section
      {
        for (i = M / 2; i < M; i++) {
          for (j = 0; j < M; j++)
            c[i] += (A[i][j] * b[j]);
          //printf("rank= %d i= %d c[%i]=%.2f\n", rank, i, i, c[i]);
        }
      }
    }
    if (rank == 0) {
      size = omp_get_num_threads();
      printf("Number of threads = %d\n", size);
    }
  }
  clock_t end_time =  clock();
  double sec = (double)(end_time - start_time) / CLOCKS_PER_SEC;
  printf("\nTime: %f", sec);
  return 0;
}
