//
// Created by main on 08.05.2021.
//
#include <mpi.h>
#include <stdio.h>

#define PI 3.14159265358

double f(double x) { return 1 / (1 + x * x); }

int main(int argc, char *argv[]) {
  double pi, sum1 = 0, sum2 = 0, term, h, t1, t2, dt;
  int myrank, nprocs, n, i;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  if (myrank == 0) {
    printf("Number of iterations=");
    scanf("%d", &n);
  }
  t1 = MPI_Wtime();
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  h = 1.0 / n;
  for (i = myrank + 1; i <= n; i += nprocs) {
    sum1 += f(h * i);
  }
  for (i = myrank + 1; i <= n; i += nprocs) {
    sum2 = sum2 + f((i - 1/2)*h);
  }
  term = ((4 * h) / 3) * (((f(0) - f(1)) / 2) + 2 * sum2 + sum1);
  MPI_Reduce(&term, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  t2 = MPI_Wtime();
  if (myrank == 0)
    printf("Pi = %lf\nComputed fault of pi = %lf\nTime: %f\n", pi, pi - PI,
           t2 - t1);
  MPI_Finalize();
  return 0;
}